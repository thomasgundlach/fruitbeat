# Copyright (c) 2014 Adafruit Industries
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
"""
 # Clientpart created on Sun May 15 21:24:21 2016
 #
 # This client represent the FruitBeat-Service with an socketbased client over an json interface,
 # the database about the sounds and instruments with load- and save operations as the
 # an interface to Capacitive Sensor to determine if a fruit was touched or not.
 # Further there is one more interface to play the mapped sounds on an touch-event.
 #
 # Python Version: 3.5.1
 # Entwicklungsumgebung: Anaconda 4.0.0 (64-bit)
 # Betriebssystem: Win64
 #
 # @category   Socket-Client, Drumkit, Capacitive Sensor
 # @package    fruitbeat
 # @author     Dennis Schroeder <dennis.schroeder@haw-hamburg.de>, Thomas Gundlach <thomas.gundlach@haw-hamburg.de>
 # @copyright  GNU
 # @version    3.0
 #
"""

import sys
import time
import pygame
import threading
import json
import Adafruit_MPR121.MPR121 as MPR121
from sys import argv
import os
from socketIO_client import SocketIO, BaseNamespace

"""
 # This function load the drumkits (represented by folders) and their sounds from the
 # given directory. It appends for every drumkit a literal list in and dictionary and
 # return the dictionary.
 # 
 # @param String audioFolder,the path to the folder where the drumkits located
 # 
 # @return: dict constructor, a dictionary over all drumkits indexed by drumkits name with
 #                            a list for each drumkit for their sounds
"""
def getSounds(audioFolder):
    drumlist = {}
    for drumkit in next(os.walk(audioFolder))[1]:
        soundlist = []
        #String-Concat to create path
        for sound in next(os.walk("%s%s/" % (audioFolder, drumkit)))[2]:
            #cut file extension
            soundlist.append(sound[:sound.rfind(".")])
        drumlist.update({drumkit : sorted(soundlist)})
    return drumlist;

# load instruments from json-database
with open('instruments.json') as jsonfile:
    instruments = json.load(jsonfile)

web_sounds = getSounds("sounds/")
# flag to determine if already reconnected after disconnect
alreadyReconnected = False

"""
 # This class is for handling connections and request from the NodeJS-Namespace
 # 
 # @param BaseNamespace BaseNamespace, Socket-Namespace of FruitBeat
 # 
"""
class Fruitspace(BaseNamespace):
    
    """
    # This method is an eventlistener which listing for ping-pong heartbeats and send
    # them back to the sender (KeepAlive-Method).
    # 
    # @param BaseNamespace self, reference to the class itself
    # @param JSON data, JSON Object with the sended data
    # 
    # @return: -
    """
    def on_heartbeat( self, data ):
        print("Receive heartbeat")
        fruitSocket.emit('heartbeat', {})

    """
    # This method is an eventlistener which listing for Sound-Change-Requests from the clients
    # by the server.
    # It save the sound to the datastore, to the mixer and reply the success.
    # 
    # @param BaseNamespace self, reference to the class itself
    # @param JSON data, JSON Object with the sended data
    # 
    # @return: -
    """
    def on_change_sound( self, data ):
        print("Receive change_sound")
        instruments[int(data['index'])]['kit'] = data['kit']
        instruments[int(data['index'])]['sound'] = data['sound']
        print("Debug: data integer instrument is: ",int(data['index']))
        print("Debug: data integer sound is: ",int(data['sound']))
        sounds[int(data['index'])] =  pygame.mixer.Sound("./sounds/" + data['kit'] + "/" + web_sounds[data['kit']][int(data['sound'])] + ".wav")
        sounds[int(data['index'])].set_volume(0.5);
        
        # save updated datastore to JSON-Database
        with open('instruments.json', 'w') as outfile:
            json.dump(instruments, outfile, sort_keys = True, indent = 4,ensure_ascii=False)    
        fruitSocket.emit('sound_changed', data)
            
    """
    # This method is an eventlistener which listing for reconnectevents and check
    # if the same reconnectevent has already thrown.
    # If reconnect is valid, it sends the login- and initialize-commands.
    # 
    # @param BaseNamespace self, reference to the class itself
    # @param JSON data, JSON Object with the sended data
    # 
    # @return: -
    """                
    def on_reconnect( self ):
        global alreadyReconnected
        if(not alreadyReconnected):
            alreadyReconnected = True
            fruitSocket.emit('sign_on', { "hash" : argv[2]})
            fruitSocket.emit('init_db', { "instruments" : instruments, "sounds" : web_sounds })
            print("Receive reconnect - reinit")
        else:
            print("Receive reconnect - nothing send")

    """
    # This method is an eventlistener which listing for a closeevent and mark the Connection-Close
    # by unset the Reconnect-Flag.
    # 
    # @param BaseNamespace self, reference to the class itself
    # @param JSON data, JSON Object with the sended data
    # 
    # @return: -
    """
    def on_close( self ):
        global alreadyReconnected
        alreadyReconnected = False
        print("Receive Close")

    """
    # This method is an eventlistener which listing for a disconnectevent and mark the Disconnect
    # by unset the Reconnect-Flag.
    # 
    # @param BaseNamespace self, reference to the class itself
    # @param JSON data, JSON Object with the sended data
    # 
    # @return: -
    """
    def on_disconnect( self ):
        global alreadyReconnected
        alreadyReconnected = False
        print("Receive disconnect")

    """
    # This method is an callback-function which called when the class will be destoryed/script exits.
    # Its use for information-use only.
    # 
    # @param BaseNamespace self, reference to the class itself
    # @param int exc_type, exitcode
    # @param int exc_value, exitvalue
    # @param String traceback, tracestack if available
    # 
    # @return: -
    """
    def __exit__(self, exc_type, exc_value, traceback):
        print("In exit")
    """
    # This method is an eventlistener which listing for a connectevent.
    # Its use for information-use only.
    # 
    # @param BaseNamespace self, reference to the class itself
    # 
    # @return: -
    """        
    def on_connect( self ):
        print ("Debug: In on_connect callback")


# Thanks to Scott Garner & BeetBox!
# https://github.com/scottgarner/BeetBox/
print ('Adafruit MPR121 Capacitive Touch Audio Player Test')

# Create MPR121 instance.
cap = MPR121.MPR121()

# Initialize communication with MPR121 using default I2C bus of device, and
# default I2C address (0x5A).  On BeagleBone Black will default to I2C bus 0.
if not cap.begin():
    print('Error initializing MPR121.  Check your wiring!')
    sys.exit(1)

# Alternatively, specify a custom I2C address such as 0x5B (ADDR tied to 3.3V),
# 0x5C (ADDR tied to SDA), or 0x5D (ADDR tied to SCL).
#cap.begin(address=0x5B)

# Also you can specify an optional I2C bus with the bus keyword parameter.
#cap.begin(busnum=1)
pygame.mixer.pre_init(44100, -16, 2, 256)
pygame.init()

# check if fruitbeat-client was started with the correct parameters...
if len(argv) > 1 and argv[1] == "-p":
    #Setup Socket with destination
    socketIO = SocketIO('https://mobile.mt.haw-hamburg.de')
    
    #set the namespace referenced to the class
    fruitSocket = socketIO.define(Fruitspace, '/fruitbeat')
    
    # send login- and initialize-commands for new connection
    fruitSocket.emit('sign_on', { "hash" : argv[2]})
    fruitSocket.emit('init_db', { "instruments" : instruments, "sounds" : web_sounds })
    print("Wait for Connect..")
    
    #Start Connection for unblocking in a Thread
    wst = threading.Thread(target=socketIO.wait, args=())
    wst.daemon = True
    wst.start()
    #exit on unvalid parameters
else:
    print("Usage:", argv[0], "-p <password>")
    sys.exit(1)

# Define mapping of capacitive touch pin presses to sound files
# tons more sounds are available in /opt/sonic-pi/etc/samples/ and
# /usr/share/scratch/Media/Sounds/
sounds = [0,0,0,0,0,0,0,0,0,0,0,0]

# initialize the audio-mixer with the current sound-set
inst_count = 0
while inst_count < len(instruments):
        sounds[inst_count] =  pygame.mixer.Sound("./sounds/" + instruments[inst_count]['kit'] + "/" + web_sounds[instruments[inst_count]['kit']][instruments[inst_count]['sound']] + ".wav")
        sounds[inst_count].set_volume(0.5);
        inst_count += 1

# Main loop to print a message every time a pin is touched.
print('Press Ctrl-C to quit.')
last_touched = cap.touched()
while True:
    current_touched = cap.touched()
    # Check each pin's last and current state to see if it was pressed or released.
    for i in range(12):
        # Each pin is represented by a bit in the touched value.  A value of 1
        # means the pin is being touched, and 0 means it is not being touched.
        pin_bit = 1 << i
        # First check if transitioned from not touched to touched.
        if current_touched & pin_bit and not last_touched & pin_bit:
            print('{0} touched!'.format(i))
            if (sounds[i]):
                sounds[i].play()
        if not current_touched & pin_bit and last_touched & pin_bit:
            print('{0} released!'.format(i))

    # Update last state and wait a short period before repeating.
    last_touched = current_touched
    time.sleep(0.02)

    # Alternatively, if you only care about checking one or a few pins you can
    # call the is_touched method with a pin number to directly check that pin.
    # This will be a little slower than the above code for checking a lot of pins.
    #if cap.is_touched(0):
    #    print('Pin 0 is being touched!')

    # If you're curious or want to see debug info for each pin, uncomment the
    # following lines:
    #print('\t\t\t\t\t\t\t\t\t\t\t\t\t 0x{0:0X}'.format(cap.touched()))
    #filtered = [cap.filtered_data(i) for i in range(12)]
    #print('Filt:', '\t'.join(map(str, filtered)))
    #base = [cap.baseline_data(i) for i in range(12)]
    #print('Base:', '\t'.join(map(str, base)))
