/*
 * Created on Sun May 15 10:00:38 2016
 *
 * This is the client script to connect a mobile- or desktop device over a NodeJs-Server
 * to FruitBeat-Service. The client offers a drag-and-drop interface to place new sounds
 * represented by draggable images to the connected fruits. It also update the interface
 * if some other also connect to FruitBeat-Service and change a sound or if the connection
 * to the FruitBeat-Service get lost.
 * Further the avaiable fruits can selected by a sliding rotator.
 *
 * NodeJS Version: v4.4.4
 * Development Environment: Eclipse Standard/SDK Kepler Service Release 2
 * Operating System: Win64
 *
 * @category   Socket-Client
 * @package    fruitbeat
 * @author     Thomas Gundlach <thomas.gundlach@haw-hamburg.de>
 * @copyright  GNU
 * @version    0.39
 *
 */
var tablename = "Fruitbox",
containername = "Fruitainer",
rotator = "Fruitator",
kitname = "Soundbox",
kitleft = "SoundsLeft",
kitright = "SoundsRight",
socket,
instruments,
sounds,
dynamicStyle,
fruitImgSize,
soundImgSize,
lastOrientation = 0;

/*
 * This method create a dynamic style-container in the html header to manage alterations
 * of the display orientation/dimensions.
 * 
 * @param -
 *
 * @return: jquery-object, jquery-object-reference of the created style-container in the header
 */
function createDynamicStyle() {
	$("head").append('<style type="text/css"></style>');
	return $("head").children(':last');
}

/*
 * This method calculate the new dimensions of the attributes in the classes of the stylesheet when the viewport has changed.
 * Further it write these calculations in the existing style-container.
 * 
 * @param float orientation, 0.2486 scalefactor for portrait and 0.37313432835820895522388059701493 scalefactor for landscape
 * 
 * @return: -
 */
function defineDynamicStyle(orientation) {
	lastOrientation = orientation;

	var fruitatorHeight = Math.round($(window).height()*orientation);
	var fontSelectSize = (0.2486 == orientation) ? "#DrumSelect {font-size: "+ ((Math.round($(window).width()*orientation)*0.13333333333333333333333333333333)) + "px;}\r\n" : "";
	soundImgSize = Math.round(fruitatorHeight/2);
	fruitImgSize = Math.round(fruitatorHeight/3)*2;
	dynamicStyle.html(fontSelectSize + "#Fruitbox{width: " + (fruitatorHeight + 40) + "px;}\r\n#Fruitator {height: " + (fruitatorHeight * 2) + "px;}\r\n#draggable {width: " + soundImgSize + "px;height: " + soundImgSize + "px;}");
}

/*
 * This method change the drumkit by empty interface from the old one and append the given new one with appendDrumkit.
 * 
 * @param String drumkit, the name of the drumkit which should be displayed on the interface 
 * 
 * @return: -
 */
function changeKit(drumkit) {
	$('#' + kitleft).html("");
	$('#' + kitright).html("");
	appendDrumkit(drumkit);
}

/*
 * Return if the given integer n is even (true) or not (false).
 * 
 * @param int n, number to check if even or odd 
 * 
 * @return: true if even, false if odd
 */
function isEven(n) {
	return n % 2 == 0;
}

/*
 * This method appends the sounds of the given drumkit in the left and right container of the interface.
 * Further it contains the callback-function for the dragging/dragged-eventlistener, which will be append
 * on every soundimage.
 * 
 * @param String drumkit, the name of the drumkit as index for the sounds-array-database. 
 * 
 * @return: -
 */
function appendDrumkit(drumkit) {

	// callback function if sound-image dragged or dragging
	var whenDragged = function(event,ui) {
		$(".ui-draggable").not(this).css({
			height: soundImgSize,
			width: soundImgSize,
			position: 'center'
		});
	};

	// add callback-function to listeners, set attributes and
	// append sound all sounds split by even or odd on the left or right side
	for (var key in sounds[drumkit]) {
		$('<img>',{"id":"" + sounds[drumkit][key] + "", "src":"./icons/" + drumkit + "/" + sounds[drumkit][key] + ".png", "value":"" + key + "","name":""+ drumkit + ""}).draggable({
			helper: "clone",
	        start: whenDragged,
	        stop: whenDragged
		}).css({"height": soundImgSize + "px","width": soundImgSize + "px"}).appendTo($('#' + ((isEven( parseInt(key) ) ) ? kitleft : kitright) ));
	}
}

/*
 * This method append the given fruits by database to the container of the rotator.
 * It also place the current sound-image of the given fruits on the fruit-image.
 * Further it define the method which will be called if a sound change by drag-and-drop and
 * append it as callback to the listener.
 * 
 * @param -
 * 
 * @return: -
 */
function appendFruits() {

	// callback function if sound get changed by drag-and-drop
	var whenDropped = function(event, ui) {
	
		// children[0] = div-container with sound-image
		// if the div container has sound-images...
	  	if(this.children[0].children.length > 0)
	  		// delete all (if placed by unknown error more than 1)
	  		// normally only 1 sound-image is in the container  
			deleteChildrens(this.children[0]);
			
		// after that set the new sound-image on the fruit
		$(ui.draggable).clone().appendTo(this.children[0]);
		
		// save the change to the database
		instruments[$(this).attr("value")].sound = this.children[0].children[0].getAttribute("value");
		instruments[$(this).attr("value")].kit = this.children[0].children[0].getAttribute("name");
		
		// notice all other, that sound has changed
		socket.emit("change_sound", {"index": parseInt($(this).attr("value")), "kit":"" + this.children[0].children[0].getAttribute("name") + "", "sound" : parseInt(this.children[0].children[0].getAttribute("value"))});
	};
	
	// for each: first append fruit, afterwards set current sound-image on fruit
	for (var key in instruments) {
		$('<span>',{"id":"" + instruments[key].fruit + "", "name":"" + instruments[key].kit + "", "value":"" + key + ""}).css({'background-image': 'url("./icons/' + instruments[key].fruit + '.png")', "background-size" : "" + fruitImgSize + "px " + fruitImgSize + "px", 'background-repeat': 'no-repeat', 'background-position': 'center'}).appendTo($('#' + rotator));
		$('#' + instruments[key].fruit).append("<div class=\"centerImg\"><img style=\"height: " + soundImgSize + "px; width: " + soundImgSize + "px;\" src=\"./icons/" + instruments[key].kit +"/" + sounds[instruments[key].kit][instruments[key].sound] + ".png\" value=\"" + instruments[key].sound +"\"></div>");
		
		// set callback on drop-event-listener
		$('#' + instruments[key].fruit).droppable({ drop: whenDropped });
	}
}

/*
 * This method append the given drumkits of the database to drumkit-selection menu.
 * It also set the wrapper-method to change the drumkit in the interface after select a new one
 * to the onchange-listener of the selection.
 * Further it appends by appendDrumkit the sounds of the first drumkit in the left
 * and right container of the interface.
 * 
 * @param -
 * 
 * @return: -
 */
function appendKitnSounds() {
	var first = true;
	$('#DrumSelect').attr('onchange', "javascript:changeKit(this.value);");
	
	for (var drumkit in sounds) {
		if(first) {
			first = false;
			appendDrumkit(drumkit);
		}
		$('#DrumSelect').append($('<option></option>').val(drumkit).html(drumkit.replace(/-/g, ' ')));
	}
}

/*
 * This method create the interface of the webapp by reference to the given data, if the FruitBeat-Service is connected.
 * The given database will be empty, if the FruitBeat-Service is offline or not avaiable.
 * 
 * @param -
 * 
 * @return: -
 */
function createFruitBox() {
    if(instruments!==undefined && instruments.length > 0) {
    	$('#' + containername).html("");
    	$('<div>',{"id":"" + kitleft + "","align":"center"}).css({"height": 100 + "%"}).appendTo($('#' + containername));
    	$('<div>',{"id":"" + kitright + "","align":"center"}).css({"height": 100 + "%"}).appendTo($('#' + containername));
    	$('<select>',{"id":"DrumSelect"}).appendTo($('#' + containername));
    	$('<div>',{"id":"" + tablename + "","align":"center"}).prependTo($('#' + containername));
    	$('<div>',{"id":"" + rotator + ""}).appendTo($('#' + tablename));
    	appendFruits();
    	appendKitnSounds();
    } else {
    	$('#' + containername).html("Fruitbox not connected!");
    }
}

/*
 * This method remove the html-element with given element-id if it exists.
 * 
 * @param String elementid, Element-ID of the HTML-Element which has to be remove
 * 
 * @return: -
 */
function deleteElement(elementid) {
	if (document.body.contains(document.getElementById(elementid))) {
		// by JQuery
		$('#' + elementid).remove();
	}
}

/*
 * This takes care about the sliding rotator.
 * It store the settings (speed, height, sliding-size, etc.) and define the handlers
 * for the touch- and clickevents to take control over gui of the rotator. Furhter it
 * contains the functionality behind that. Moreover in a special function for that,
 * which called conditioned by some browsers with a delay.
 * 
 * @param -
 * 
 * @return: -
 */
function handleFruitator() {
	var gate = $(window),
	cog = $('#' + rotator),
	digit = cog.find('span'),
	slot = digit.eq(0).height(),
	base = 1.25*slot,
	up,debugCount = 0;
	cog.fadeTo(0,0);
	
	// browsers-conditioned delay to call interAction
	setTimeout(interAction, 50);
	
	// function for eventhandlers and functionality
	function interAction() {
	
		// set the rotator to the startposition
		cog.scrollTop(base).fadeTo(0,1).mousewheel(function(turn, delta) {
	
			if (isBusy()) return false;
	
			delta < 0 ? up = true : up = false;
	
			newNumber();
	
			return false;
		});
		
		// function to handle mouse- and touchclickevents
		var mouseTouchStart = function(e) {
			if (e.which && e.which != 1) return;
	
			var item = $(this).index();
				
			if (item == 1 || item == 3) {
	
			$(this).one('mouseup touchend', function() {
	
				var same = item == $(this).index();
	
				if (isBusy() || !same) return cancelIt($(this));
	
				item == 1 ? up = true : up = false;
	
				newNumber();
	
				return cancelIt($(this));
			});
			}
	
			return false;
		};
	
		// function to handle touchswipeevents
		digit.on('touchstart', function(e) {
	
			var touch = e.originalEvent.touches,
			begin = touch[0].pageY, swipe;
	
			digit.on('touchmove', function(e) {
	
				var contact = e.originalEvent.touches,
				end = contact[0].pageY,
				distance = end-begin;
	
				if (isBusy()) return;
	
				if (Math.abs(distance) > 30) {
				swipe = true;
				distance > 30 ? up = true : up = false;
				}
			})
			.add(gate).one('touchend', function() {
	
				if (swipe) {
				swipe = false;
				newNumber();
				}
	
				digit.off('touchmove').add(gate).off('touchend');
			});
		})
		.on('mousedown touchstart', mouseTouchStart);
	}
	
	// returns the state if the rotator still scrolling or not
	function isBusy() {
	
		return cog.is(':animated');
	}
	
	// to cancel an eventaction if rotator is busy
	function cancelIt(element) {
	
		element.off('mouseup touchend');
	
		return false;
	}
	
	// function to move the rotator
	function newNumber() {
		
		var aim = base;
	
		up ? aim -= slot : aim += slot;
	
		cog.animate({scrollTop: aim}, 250, function() {
	
			up ? digit.eq((instruments.length-1)).prependTo(cog) : digit.eq(0).appendTo(cog);
			cog.scrollTop(base);
	
			digit = cog.find('span');
		});
	}
}

/*
 * This method remove all children-elements of the given dom-element.
 * 
 * @param -
 * 
 * @return: -
 */
function deleteChildrens(element) {
		for(var i=0; i<element.children.length; i++) {
			if (document.body.contains(element.children[i])) {
				element.removeChild(element.children[i]);
			}
		}
}

/*
 * This jQuery-method inizialize the client on load and make sure to connect to the
 * NodeJS-Service. It also start some events to listen to commands of the client-server-connection
 * or changing the display orientation.
 *  
 * @param -
 *
 * @return: -
 */
$(function() {

	dynamicStyle = createDynamicStyle();
	socket = io.connect('/fruitbeat');
	
	// event listen for orientation change
	$( window ).on('resize',
		function() {
			// redraw gui only when orientation changed
			if(lastOrientation != (($( window ).height() > $( window ).width()) ? 0.2486 : 0.37313432835820895522388059701493)) {
			// if orientation changed...
				// redefine stylesheet by new dimensions (responsive design)
				defineDynamicStyle(
					($( window ).height() > $( window ).width()) ? 0.2486 : 0.37313432835820895522388059701493 
				);
				// clear the old interface..
				deleteElement(tablename);
				deleteElement("DrumSelect");
				deleteElement(kitleft);
				deleteElement(kitright);
				
				// redraw the interface with new dimensions
				createFruitBox();
				handleFruitator();
			}
		}
	);
	
	// inizialize dynamic stylesheet on load
	setTimeout(
		defineDynamicStyle(
			($( window ).height() > $( window ).width()) ? 0.2486 : 0.37313432835820895522388059701493 
		),
	50);
	
	// listen for initialization or reinitialization of the instruments/sound-database
	socket.on("init_db", function(data){
		instruments = data.instruments;
		sounds = data.sounds;
		
		// If fruitbox/soundbox already initialize, remove it
		deleteElement(tablename);
		deleteElement("DrumSelect");
		deleteElement(kitleft);
		deleteElement(kitright);
		
		// draw or redraw it with the new data
		createFruitBox();
		handleFruitator();
	});
	
	// listen alterations of the current sounds on the fruits by other clients
	socket.on("sound_changed", function(data) {
		// Update Database
		instruments[data.index].kit = data.kit;
		instruments[data.index].sound = data.sound;

		// Update Gui
		document.getElementById(instruments[data.index].fruit).children[0].children[0].setAttribute("src","./icons/" + data.kit + "/" + sounds[data.kit][data.sound] + ".png");
		document.getElementById(instruments[data.index].fruit).children[0].children[0].setAttribute("value", data.sound);
		document.getElementById(instruments[data.index].fruit).children[0].children[0].setAttribute("name", data.kit);
		document.getElementById(instruments[data.index].fruit).children[0].children[0].setAttribute("id", sounds[data.kit][data.sound]);
	});
});