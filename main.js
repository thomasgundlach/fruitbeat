/*
 * Created on Tue May 17 17:01:43 2016
 *
 * This is the server script to listen for incoming connections from clients or the FruitBeat-Service.
 * Its the interface between all clients and the FruitBeat-Service. It valid request and forward them.
 * It hold the last database-version and distribute it to all clients expect the FruitBeat-Service.
 * Further it contains methods to registrate users and to make a distinction between users and admins.
 * It control the Connection to the FruitBeat-Service by ping and hold an external visible module
 * for the former server-script of all namespaces.
 *
 * NodeJS Version: v4.4.4
 * Development Environment: Eclipse Standard/SDK Kepler Service Release 2
 * Operating System: Win64
 *
 * @category   Socket-Server
 * @package    fruitbeat
 * @author     Thomas Gundlach <thomas.gundlach@haw-hamburg.de>
 * @copyright  GNU
 * @version    0.37
 *
 */
var PASSWORD = 'de8f8da4115b0ac3db5d65247fae295033015268e98886ae1f03c7bee3239c14';
var ADMINS = {};
var FB_ONLINE = false;
var sounds;
var instruments;
var sha256 = require('./sha256.js');
var NAMESPACE = '/fruitbeat';
var debugHeartBeat = 0;

/*
 * This functions determine if the given socket belongs to an admin or not. 
 * 
 * @param object socket, the socket object with the socket-id of the user which requested to be checked
 * 
 * @return: true if FruitBeat-Service is available, false if not
 */
function isAdmin(socket) {
	return (socket.id in ADMINS);
}

/*
 * This functions determine if wherever is or not any FruitBeat-Service registrated in the admintable. 
 * 
 * @param -
 * 
 * @return: true if FruitBeat-Service is available, false if not
 */
function hasFruitBox() {
	for (var key in ADMINS) {
		if(ADMINS[key] >= 0)
			return true;
	}
	return false;
}

/*
 * This functions removes an admin from the admintable if the connection to this client gets broken.
 * It valide if the given socket belongs to an admin. If so, it removes the user from the admintable.
 * And if the adminuser also the FruitBeat-Service, mark the FruitBeat-Service as offline, empty the database
 * and send to all users a emtpy database init which is equal to the clients as a offline Service. 
 * 
 * @param object socket, the socket object with the socket-id of the disconnected client
 * 
 * @return: -
 */
function disconnectAdmin(socket) {
	if(isAdmin(socket)) {
		clearInterval(ADMINS[socket.id]); // delete heartbeat interval send
		delete ADMINS[socket.id];
		if(!hasFruitBox()) {
			FB_ONLINE = false;
			// Clear Sound- and Instrument-DB
			sounds = [];
			instruments = [];
			// Send to all clients that no instruments and no sounds avaiable
			socket.broadcast.emit('init_db', { "instruments" : instruments, "sounds" : sounds });
			
			// Log-Debug for potential Error-Handling
			console.log("fruitbeat: Debug: Admin was Fruitbox - DB is empty");
		}

		// Log-Debug for potential Error-Handling
		console.log("fruitbeat: Debug: Admin deleted by disconnect");
	} else {
		// Log-Debug for potential Error-Handling
		console.log("fruitbeat: Debug: Admin " + socket.id + " not found on disconnect");
	}
}

/*
 * This method will be visible from outside and call by the former script which represent the main-server.
 * Its starts the FruitBeat-Server to listen for incoming Connections from clients and the FruitBeat-Service.
 * It setup some eventlisteners to control the commands of the socket-connections and react on network-status changes.
 * 
 * @param object io, the io-socket object to open a server in a special namespace
 * @param object express, an object to express instance to define express namespaces
 * @param object app, an object of the server application itself to define weblinks and their local sources
 * 
 * @return: -
 */
// Eventhandler for Connections of new Clients
module.exports = {
	initialize: function(io,express,app) {
        app.use("/fruitbeat", express.static(__dirname+'/../public/'));
		var fruitSocket = io.of(NAMESPACE);
		var HEARTBEAT = false;
		
		// Socket-Handler to manage new incoming connections
		fruitSocket.on('connection', function (socket) {
		
			// Log-Debug for potential Error-Handling
			console.log("fruitbeat: connection received of " + socket.id);
			
			// send the database to the new client
			socket.emit('init_db', { "instruments" : instruments, "sounds" : sounds });

			// Socket-Handler to manage Disconnections of the given client
			socket.on('disconnect', function(data){
				disconnectAdmin(socket);
		  	});
		  	
			// Socket-Handler to manage Connection-Close-Events of the given client
			socket.on('close', function(data){
				disconnectAdmin(socket);
		  	});
		  
			// Socket-Command-Handler to give the client the possibility to registrate itself as an admin  
			socket.on("sign_on", function(data){

				// using external sha256 to hash the password and compare it with password-hash in the datastore
				if(sha256.sha256_digest(data.hash) == PASSWORD) {
					ADMINS[socket.id] = -1;
					
					// Log-Debug for potential Error-Handling
					console.log("fruitbeat: Debug: User " + socket.id + " added to Admin");
				} else {
					// Log-Debug for potential Error-Handling
					console.log("fruitbeat: Debug: hash " + data.hash);
					console.log("fruitbeat: Debug: password " + PASSWORD);
					console.log("fruitbeat: Debug: Wrong Password for " + socket.id);
				}
			});
			
			// Socket-Handler to manage that the Connection to a connected FruitBeat-Service is still alive
			socket.on("heartbeat", function(data){
				HEARTBEAT = true;
				debugHeartBeat++;
			});
		
			// Socket-Command-Handler to receive the Instruments- and Sounddatabase from a connected FruitBeat-Service
			socket.on("init_db", function(data){
				if(isAdmin(socket)) {
					HEARTBEAT = true;
					FB_ONLINE = socket.id; // Fruitbox is online
					//setup ping-pong to a FruitBeat-Service, to ensure, that the Connection is still alive 
					ADMINS[socket.id] = setInterval(function() {
						if(!HEARTBEAT) {
							// Log-Debug for potential Error-Handling
							console.log("fruitbeat: heartbeat false");
							clearInterval(ADMINS[socket.id]);
			    			socket.disconnect();
			    		} else {
			    			HEARTBEAT = false;
							fruitSocket.to(socket.id).emit('heartbeat', {});
			    		}
					}, 5000);
					
					// Save the jsonbased Instruments- and Sounddatabase in local datastore
					sounds = data.sounds;
					instruments = data.instruments;
					
					// Spread the Database to all connected Clients
					socket.broadcast.emit('init_db', data);
				}
			});
			
			// Socket-Command-Handler to validate, manage and forward a Sound-Change-Request to the FruitBeat-Service
			socket.on("change_sound", function(data) {
			
					// Log-Debug for potential Error-Handling
					console.log("fruitbeat: Debug: change_sound received");
				if(FB_ONLINE && data.hasOwnProperty('index') && data.hasOwnProperty('kit') && data.hasOwnProperty('sound')) {

					var fruitindex = parseInt(data.index),
						soundindex = parseInt(data.sound);

					// validate data
					if(!isNaN(fruitindex) && instruments.length > fruitindex && fruitindex >= 0 && !isNaN(soundindex) && soundindex >= 0 && sounds.hasOwnProperty(data.kit) && sounds[data.kit].length > soundindex) {
						fruitSocket.to(FB_ONLINE).emit('change_sound', data);
						
						// Log-Debug for potential Error-Handling
						console.log("fruitbeat: Debug: change_sound forwarded");
					} else {
						// Log-Debug for potential Error-Handling
						console.log("fruitbeat: Debug: \t invalid data: " + data);
					}

				} else {
					// Log-Debug for potential Error-Handling
					console.log("fruitbeat: Debug: \t Fruitbox not online or propertys not fit:" + data);
				}
			});
			
			// Socket-Command-Handler to let the FruitBeat-Service approve a Sound-Change-Request
			// and spread it to all other Clients
			socket.on("sound_changed", function(data) {
				if(isAdmin(socket)) {
					instruments[data.index].kit = data.kit;
					instruments[data.index].sound = data.sound;
					socket.broadcast.emit('sound_changed', data);
					
					// Log-Debug for potential Error-Handling
					console.log("fruitbeat: Debug: sound have changed and spreaded");
				}
			});
		});
	},
	
	// External function to represent the authors of FruitBeat to the main-plattform
    getAuthors: function(){
            return ["Dennis Schr\u00f6der", "Antonia Schwab", "Thomas Gundlach"];
    },
	// External function to give a short description about the project on the main-plattform
    getInformation: function(){
            return "Kreiere fette Beats, epische Star Wars Soundscapes oder tierische Laute. Mit Obst!";
    },
	// External function to supply the main-plattform with a working title
    getTitle: function(){
            return "FruitBeat";
    }

};